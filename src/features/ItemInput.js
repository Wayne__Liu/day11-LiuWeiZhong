import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "./ItemSlice";
import { nanoid } from "nanoid";

const ItemInput = () => {
  const dispatch = useDispatch();
  const [todoTitle, setTodoTitle] = useState('');

  const handleInputChange = (event) => {
    setTodoTitle(event.target.value);
  };

  const handleChange = () => {
    todoTitle && dispatch(addTodo({
      id: nanoid(),
      title: todoTitle,
      done: false
    }));
    setTodoTitle('');
  };

  return (
    <div className="item-input-container">
      <input type="text" value={todoTitle} onChange={handleInputChange} />
      <button onClick={handleChange}>add</button>
    </div>
  );
};

export default ItemInput;
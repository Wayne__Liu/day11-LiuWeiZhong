import { useDispatch } from "react-redux";
import { deleteTodo, changeTodoDone } from "./ItemSlice";

const Item = (props) => {
    const dispatch = useDispatch();

    const handleDelete = (id) => {
        dispatch(deleteTodo(id));
    };

    const changeTodoDoneState = () => {
        dispatch(changeTodoDone(props.todo.id));
    };

    const { todo } = props;
    return (
        <div>
            <span
                onClick={changeTodoDoneState}
                style={{ textDecoration: todo.done ? "line-through" : "none" }}
            >
                {todo.title}
            </span>
            <button onClick={() => handleDelete(todo.id)}>❌</button>
        </div>
    );
};

export default Item;

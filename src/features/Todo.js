import ItemInput from "./ItemInput";
import ItemList from "./ItemList";

const Todo = () => {
  return (
    <div className="todo-container">
      <div>Todo List</div>
      <ItemList />
      <ItemInput />
    </div>
  );
};

export default Todo;
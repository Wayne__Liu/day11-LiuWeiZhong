import { createSlice } from "@reduxjs/toolkit";

export const ItemSlice = createSlice({
    name: 'todo',
    initialState: {
        ItemList: [],
    },

    reducers: {
        addTodo: (state, action) => {
            const newTodo = action.payload;
            state.ItemList.push(newTodo);
        },
        changeTodoDone: (state, action) => {
            const id = action.payload;
            state.ItemList.forEach(todo => {
                if (todo.id === id) {
                    todo.done = !todo.done;
                }
            })

        },
        deleteTodo: (state, action) => {
            state.ItemList = state.ItemList.filter(todo => todo.id !== action.payload)
        },
    }
})

export const { addTodo, deleteTodo, changeTodoDone } = ItemSlice.actions

export default ItemSlice.reducer
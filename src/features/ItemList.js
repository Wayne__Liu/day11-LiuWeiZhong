import { useSelector } from "react-redux";
import Item from "./Item";

const ItemList = () => {
    const todoList = useSelector((state) => state.todo.ItemList);

    return (
        <div>
            {todoList.map((todo) => {
                return <Item todo={todo} key={todo.id} />;
            })}
        </div>
    );
};

export default ItemList;
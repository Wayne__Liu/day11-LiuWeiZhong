import { configureStore } from '@reduxjs/toolkit'
import todoReducer from '../features/ItemSlice'

export default configureStore({
  reducer: {todo: todoReducer},
})